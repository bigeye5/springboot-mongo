package za.co.limehouse.tuts.springbootmongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.limehouse.tuts.springbootmongo.domain.Uom;
import za.co.limehouse.tuts.springbootmongo.repository.UomRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UomServiceImpl implements UomService {

    private UomRepository uomRepository;

    @Autowired
    public UomServiceImpl(UomRepository uomRepository) {
        this.uomRepository = uomRepository;
    }

    @Override
    public List<Uom> findAll() {
        return uomRepository.findAll();
    }

    @Override
    public Optional<Uom> findById(String id) {
        return uomRepository.findById(id);
    }

    @Override
    public Optional<List<Uom>> search(Map<String, String> searchFilter) {
        String searchTerm = searchFilter.get("description");
        // this could be multiple params with logic to decide what to call...
        return uomRepository.findByDescriptionContainingIgnoreCase(searchTerm);
    }

    @Override
    public Uom insert(Uom uom) {
        return  uomRepository.insert(uom);
    }

    @Override
    public Uom update(Uom uom) {
        return uomRepository.save(uom);
    }

    @Override
    public void deleteById(String id) {
        uomRepository.deleteById(id);
    }

}

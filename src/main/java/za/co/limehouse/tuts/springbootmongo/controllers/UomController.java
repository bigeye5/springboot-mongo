package za.co.limehouse.tuts.springbootmongo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import za.co.limehouse.tuts.springbootmongo.Constants;
import za.co.limehouse.tuts.springbootmongo.domain.Uom;
import za.co.limehouse.tuts.springbootmongo.model.UomDto;
import za.co.limehouse.tuts.springbootmongo.service.UomService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/" + Constants.VERSION)
@Slf4j
public class UomController {
    private UomService uomService;

    @Autowired
    public UomController(UomService uomService) {
        this.uomService = uomService;
    }

    @GetMapping("/uom")
    public List<UomDto> findAll() {
        log.debug("findAll was called");
        List<Uom> uoms = uomService.findAll();
        if (uoms.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND);
        } else {
            return uoms.stream().map(u -> new UomDto(u)).collect(Collectors.toList());
        }
    }

    @GetMapping("/uom/{id}")
    public UomDto findById(@PathVariable String id) {
        log.debug("Finding UOM: {}", id);
        Optional<Uom> uom = uomService.findById(id);
        if (uom.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND);
        } else {
            return new UomDto(uom.get());
        }
    }

    @PostMapping("/uom/search")
    public List<UomDto> search(@RequestBody Map<String, String> searchFilter) {

        if (isValidSearch(searchFilter)) {
            Optional<List<Uom>> uoms = uomService.search(searchFilter);
            if (uoms.get().isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND);
            } else {
                return uoms.get().stream().map(u -> new UomDto(u)).collect(Collectors.toList());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
        }
    }

    @PostMapping("/uom")
    public UomDto insert(@RequestBody Map<String, String> body) {
        Uom uom = new Uom();
        uom.setDescription(body.get("description"));
        if (isValidInsert(uom)) {
            return new UomDto(uomService.insert(uom));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
        }
    }

    @PutMapping("/uom")
    public UomDto update(@RequestBody Map<String, String> body) {

        Optional<Uom> uom = uomService.findById(body.get("id"));
        if (uom.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND);
        } else {
            Uom unitOfMeasure = uom.get();
            unitOfMeasure.setDescription(body.get("description"));
            if (isValidUpdate(unitOfMeasure)) {
                return new UomDto(uomService.update(unitOfMeasure));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
            }
        }
    }

    @DeleteMapping("/uom/{id}")
    public void delete(@PathVariable String id) {
        Optional<Uom> uom = uomService.findById(id);
        if (uom.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND);
        } else {
            uomService.deleteById(id);
        }
    }

    private boolean isValidInsert(Uom uom) {
        return uom.getDescription().isBlank() ? false : true;
    }

    private boolean isValidUpdate(Uom uom) {
        if (uom.getId().isBlank() || uom.getId().length() != Constants.UUID_LENGTH
                || uom.getDescription().isBlank()) {
            return false;
        }
        return true;
    }

    private boolean isValidSearch(Map<String, String> searchFilter) {
        return searchFilter.containsKey("description");
    }

}

package za.co.limehouse.tuts.springbootmongo.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "uom")
public class Uom {

    private String id;
    private String description;
}

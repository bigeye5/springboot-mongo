package za.co.limehouse.tuts.springbootmongo.service;

import za.co.limehouse.tuts.springbootmongo.domain.Uom;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UomService {

    List<Uom> findAll();
    Uom insert(Uom uom);
    Uom update(Uom uom);
    void deleteById(String id);
    Optional<Uom> findById(String id);
    Optional<List<Uom>> search(Map<String, String> searchFilter);
}

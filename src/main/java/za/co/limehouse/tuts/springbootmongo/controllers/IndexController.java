package za.co.limehouse.tuts.springbootmongo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @GetMapping("/")
    public String index() {
        return "Hello, this is the Spring Boot Mongo Index endpoint. Try using resource endpoints :-)";
    }
}

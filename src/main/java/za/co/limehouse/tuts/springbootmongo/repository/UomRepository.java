package za.co.limehouse.tuts.springbootmongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.limehouse.tuts.springbootmongo.domain.Uom;

import java.util.List;
import java.util.Optional;

public interface UomRepository extends MongoRepository<Uom, String> {
    Optional<List<Uom>> findByDescriptionContainingIgnoreCase(String description);
}
